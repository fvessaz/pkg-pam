From: Sam Hartman <hartmans@debian.org>
Date: Sun, 15 Sep 2002 21:19:55 +0000
Subject: _modules_pam_limits_chroot

---
 modules/pam_limits/limits.conf       |  2 ++
 modules/pam_limits/limits.conf.5.xml |  6 ++++++
 modules/pam_limits/pam_limits.c      | 25 ++++++++++++++++++++++---
 3 files changed, 30 insertions(+), 3 deletions(-)

diff --git a/modules/pam_limits/limits.conf b/modules/pam_limits/limits.conf
index be621a7..ed3ceda 100644
--- a/modules/pam_limits/limits.conf
+++ b/modules/pam_limits/limits.conf
@@ -35,6 +35,7 @@
 #        - msgqueue - max memory used by POSIX message queues (bytes)
 #        - nice - max nice priority allowed to raise to values: [-20, 19]
 #        - rtprio - max realtime priority
+#        - chroot - change root to directory (Debian-specific)
 #
 #<domain>      <type>  <item>         <value>
 #
@@ -45,6 +46,7 @@
 #@faculty        soft    nproc           20
 #@faculty        hard    nproc           50
 #ftp             hard    nproc           0
+#ftp             -       chroot          /ftp
 #@student        -       maxlogins       4
 
 # End of file
diff --git a/modules/pam_limits/limits.conf.5.xml b/modules/pam_limits/limits.conf.5.xml
index 380a139..d59b1a8 100644
--- a/modules/pam_limits/limits.conf.5.xml
+++ b/modules/pam_limits/limits.conf.5.xml
@@ -266,6 +266,12 @@
                   (Linux 2.6.12 and higher)</para>
               </listitem>
             </varlistentry>
+            <varlistentry>
+              <term><option>chroot</option></term>
+              <listitem>
+                <para>the directory to chroot the user to</para>
+              </listitem>
+            </varlistentry>
           </variablelist>
         </listitem>
       </varlistentry>
diff --git a/modules/pam_limits/pam_limits.c b/modules/pam_limits/pam_limits.c
index 4bc4ae7..43196e8 100644
--- a/modules/pam_limits/pam_limits.c
+++ b/modules/pam_limits/pam_limits.c
@@ -88,6 +88,7 @@ struct pam_limit_s {
     int flag_numsyslogins; /* whether to limit logins only for a
 			      specific user or to count all logins */
     int priority;	 /* the priority to run user process with */
+    char chroot_dir[8092]; /* directory to chroot into */
     struct user_limits_struct limits[RLIM_NLIMITS];
     const char *conf_file;
     int utmp_after_pam_call;
@@ -98,6 +99,7 @@ struct pam_limit_s {
 #define LIMIT_NUMSYSLOGINS RLIM_NLIMITS+2
 
 #define LIMIT_PRI RLIM_NLIMITS+3
+#define LIMIT_CHROOT RLIM_NLIMITS+4
 
 #define LIMIT_SOFT  1
 #define LIMIT_HARD  2
@@ -484,6 +486,8 @@ static int init_limits(pam_handle_t *pamh, struct pam_limit_s *pl, int ctrl)
     pl->login_limit = -2;
     pl->login_limit_def = LIMITS_DEF_NONE;
 
+    pl->chroot_dir[0] = '\0';
+    
     return retval;
 }
 
@@ -554,6 +558,8 @@ process_limit (const pam_handle_t *pamh, int source, const char *lim_type,
 	pl->flag_numsyslogins = 1;
     } else if (strcmp(lim_item, "priority") == 0) {
 	limit_item = LIMIT_PRI;
+    } else if (strcmp(lim_item, "chroot") == 0) {
+	limit_item = LIMIT_CHROOT;
     } else {
         pam_syslog(pamh, LOG_DEBUG, "unknown limit item '%s'", lim_item);
         return;
@@ -591,9 +597,9 @@ process_limit (const pam_handle_t *pamh, int source, const char *lim_type,
 			pam_syslog(pamh, LOG_DEBUG,
 				   "wrong limit value '%s' for limit type '%s'",
 				   lim_value, lim_type);
-            return;
+			return;
 		}
-	} else {
+	} else if (limit_item != LIMIT_CHROOT) {
 #ifdef __USE_FILE_OFFSET64
 		rlimit_value = strtoull (lim_value, &endptr, 10);
 #else
@@ -654,7 +660,11 @@ process_limit (const pam_handle_t *pamh, int source, const char *lim_type,
 #endif
     }
 
-    if ( (limit_item != LIMIT_LOGIN)
+    if (limit_item == LIMIT_CHROOT) {
+	strncpy(pl->chroot_dir, value_orig, sizeof(pl->chroot_dir)-1);
+        pl->chroot_dir[sizeof(pl->chroot_dir)-1]='\0';
+    }
+    else if ( (limit_item != LIMIT_LOGIN)
 	 && (limit_item != LIMIT_NUMSYSLOGINS)
 	 && (limit_item != LIMIT_PRI) ) {
         if (limit_type & LIMIT_SOFT) {
@@ -998,6 +1008,15 @@ static int setup_limits(pam_handle_t *pamh,
         retval |= LOGIN_ERR;
     }
 
+    if (!retval && pl->chroot_dir[0]) {
+	i = chdir(pl->chroot_dir);
+	if (i == 0)
+	    i = chroot(pl->chroot_dir);
+	if (i == 0)
+	    i = chdir("/");
+	if (i != 0)
+	    retval = LIMIT_ERR;
+    }
     return retval;
 }
 
